import numpy as np
import matplotlib.pyplot as plt
''''trigonometria'''
sin=np.sin
cos=np.cos
tan=np.tan
arcsin=np.arcsin
arccos=np.arccos
arctan=np.arctan
'''log'''
ln=np.log
log=np.log
log10=np.log10
log2=np.log2
esp=np.exp
'''costanti'''
inf=np.inf
infn=np.NINF
pi=np.pi
e=np.e
####################################################################
def calcolaRettangoli(x,y,deltax):
    A=0
    for i in range(x.size):
        A+=y[i]*deltax #AREA RETTANGOLO
    return A
def calcolaTrapezi(a,t,deltax,funz):
    funz_a=funz.replace('x','a')
    A=0
    for i in range(t.size):
        y1=eval(funz_a)
        x1=a
        a+=deltax
        y2=eval(funz_a)
        x2=a
        A+=((y2+y1)*deltax)/2#AREA TRAPEZIO
    return A
####################################################################
funz=input("Dimmi la funzione:\n")
a=float(input('a:\n'))
b=float(input('b:\n'))
massimon=500
funzrisultato=(input('Dimmi il risultato'))
risultato=eval(funzrisultato)

areeRett=np.zeros(massimon)
areeTrap=np.zeros(massimon)

for n in range(1,massimon+1):
    d=(b-a)/n
    x=np.arange(a,b,d)
    y=eval(funz)
    areeRett[n-1]=calcolaRettangoli(x,y,d)
    areeTrap[n-1]=calcolaTrapezi(a,x,d,funz)

valorin=np.arange(1,massimon+1,1)

print('Differenza tra valore vero e rettangoli con n='+str(massimon)+': '+str(abs(areeRett[-1]-risultato)))
print('Differenza tra valore vero e trapezi con n='+str(massimon)+': '+str(abs(areeTrap[-1]-risultato)))


fig, ax = plt.subplots(nrows=1,ncols=2)
ax[0].plot(valorin,areeRett,color='green')
ax[0].plot(valorin,areeTrap,color='red') #comparazione dei due metodi
ax[0].grid()
diffTrapRett=np.abs(areeTrap-areeRett)
ax[1].plot(valorin,diffTrapRett,color='orange')##differenza tra un metodo e l'altro all'aumentare di n
ax[1].grid()
plt.show()

    
