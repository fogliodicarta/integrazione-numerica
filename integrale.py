import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
''''trigonometria'''
sin=np.sin
cos=np.cos
tan=np.tan
arcsin=np.arcsin
arccos=np.arccos
arctan=np.arctan
'''log'''
ln=np.log
log=np.log
log10=np.log10
log2=np.log2
esp=np.exp
sqrt=np.sqrt
'''costanti'''
inf=np.inf
infn=np.NINF
pi=np.pi
e=np.e
######################################################################
def disegnaRettangoli(assi,t,z,deltax):
    A=0
    for i in range(t.size):
        rect = patches.Rectangle((t[i],0),deltax,z[i],linewidth=1,edgecolor='#FF000069',facecolor='#FF000069')
        assi.add_patch(rect)
        A+=z[i]*deltax #AREA RETTANGOLO
    for i in range(t.size):
        rect = patches.Rectangle((t[i],0),deltax,z[i],linewidth=1,edgecolor='r',facecolor='none')
        assi.add_patch(rect)
    return A
        
def disegnaTrapezi(assi,a,t,deltax,funz):
    funz_a=funz.replace('x','a')
    A=0
    for i in range(t.size):
        y1=eval(funz_a)
        print(y1)
        x1=a
        a+=deltax
        y2=eval(funz_a)
        x2=a
        xvertici = [x1,x2,x2,x1]
        yvertici = [0,0,y2,y1]
        A+=((y2+y1)*deltax)/2#AREA TRAPEZIO
        assi.add_patch(patches.Polygon(xy=list(zip(xvertici,yvertici)), edgecolor='#FF0000F0',facecolor='#FF000069',linewidth=2.5))
    return A
    
#######################################################################
funz=input("Dimmi la funzione:\n")
a=float(input('a:\n'))
b=float(input('b:\n'))
nrett=float(input('Dimmi quanti intervallini vuoi:\n'))

vadd=(b-a)/5
deltax=abs(b-a)/nrett

t=np.arange(a,b,deltax)
z=eval(funz.replace('x','t'))

x=np.arange(a-vadd*5,b+0.01+vadd*5,0.001)
y=eval(funz)

fig, ax = plt.subplots(nrows=2,ncols=1)

ax[0].plot(x, y)
ax[0].axvline(x=0,color='k')
ax[0].axhline(y=0,color='k')

ax[0].axvline(x=a,color='g',linestyle='--')
ax[0].axvline(x=b,color='g',linestyle='--')

ARett=disegnaRettangoli(ax[0],t,z,deltax)
xmin=a
xmax=b
ymin=abs(a)*-1
ymax=abs(a)
ax[0].set(xlim=(xmin, xmax), ylim=(ymin, ymax))


ax[0].set_aspect(1)
ax[0].set(xlabel='x', ylabel='y',title=funz+" metodo dei rettangoli")

#ax[0].set_xlim(a-vadd, b+vadd)
#ax[0].set_ylim(-10, 10)
ax[0].grid()

ax[1].plot(x, y)
ax[1].axvline(x=0,color='k')
ax[1].axhline(y=0,color='k')

ax[1].axvline(x=a,color='g',linestyle='--')
ax[1].axvline(x=b,color='g',linestyle='--')

#disegnaRettangoli(ax[1],t,z,deltax)
ATrp = disegnaTrapezi(ax[1],a,t,deltax,funz)
ax[1].set(xlim=(xmin, xmax), ylim=(ymin, ymax))

print("Area calcolata con il metodo dei rettangoli: "+str(ARett))
print("Area calcolata con il metodo dei trapezi: "+str(ATrp))

print("La differenza tra i due metodi è: "+str(abs(ATrp-ARett)))


ax[1].set_aspect(1)
ax[1].set(xlabel='x', ylabel='y',title=funz+" metodo dei trapezi")

#ax[1].set_xlim(a-vadd, b+vadd)
#ax[1].set_ylim(-10, 10)
ax[1].grid()
#funz_a=funz.replace('x','a')
#funz_b=funz.replace('x','b')
#plt.axis([a, b, eval(funz_a), eval(funz_b3)])
plt.show()

